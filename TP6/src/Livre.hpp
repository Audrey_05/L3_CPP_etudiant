#ifndef Livre_HPP_
#define Livre_HPP_

#include <iostream>
#include <string>

class Livre
{
	protected: 
		std::string _titre;
		std::string _auteur;
		int _annee;
		
	public: 
		Livre();
		Livre(const std::string & titre, const std::string & auteur, int annee);
		const std::string & getTitre() const; 
		const std::string & getAuteur() const;
		int getAnnee() const; 
	
	
};



#endif
