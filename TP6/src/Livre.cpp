#include "Livre.hpp"
#include <iostream>
#include <string>

Livre::Livre():_titre(""), _auteur(""), _annee(){
}
Livre::Livre(const std::string & titre, const std::string & auteur, int annee): _titre(titre), _auteur(auteur), _annee(annee){
	if((int)_auteur.find(';') !=-1)
	{
		throw std::string("erreur : auteur non valide (';' non autorisé)");

	}
	else if((int)_auteur.find('\n') !=-1)
	{
		throw std::string("erreur : auteur non valide ('\n' non autorisé)");
	}
	else if((int)_titre.find(';') !=-1)
	{
		throw std::string("erreur : titre non valide (';' non autorisé)");
	}
	else if((int)_titre.find('\n') !=-1)
	{
		throw std::string("erreur : titre non valide ('\n' non autorisé)");

	}

}


const std::string & Livre::getTitre() const{
	return _titre;
}
const std::string & Livre::getAuteur() const{
	return _auteur; 
}
int Livre::getAnnee() const{
	return _annee;
}