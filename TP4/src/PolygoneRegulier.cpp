#include "PolygoneRegulier.hpp"
#include <iostream>
#include <cmath>

	PolygoneRegulier::PolygoneRegulier( const Couleur & couleur, const Point & centre, int rayon, int nbCotes): FigureGeometrique(couleur), _nbPoints(nbCotes)
	{
		_points = new Point[nbCotes]; 
		Point tmp; 
		float alpha;
		for(int i=0; i<nbCotes; i++)
		{
			alpha = (i*M_PI*2)/ _nbPoints;
			tmp._x = rayon* cos(alpha) + centre._x;
			tmp._y = rayon* sin(alpha) + centre._y; 
			_points[i] = tmp; 
			
		}
		
	}
	
	PolygoneRegulier::~PolygoneRegulier()
	{
		delete[]_points; 
	}
	
	void PolygoneRegulier::afficher() const 
	{
		std:: cout <<"PolygoneRegulier " <<getCouleur()._r <<"_"<< getCouleur()._g<<"_"<<getCouleur()._b<< " "; 
		for(int i=0; i<getNbPoints(); i++)
		{
			std::cout  << getPoint(i)._x << "_" << getPoint(i)._y<< " "; 
			
		}
	}
	
	int PolygoneRegulier::getNbPoints() const
	{
		return _nbPoints; 
	}
	
	const Point& PolygoneRegulier:: getPoint(int indice) const
	{
		return _points[indice]; 
	}
