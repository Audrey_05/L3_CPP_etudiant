#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "Couleur.hpp"
#include "Point.hpp"
#include "PolygoneRegulier.hpp"

#include <iostream>

int main() {
	Point centre; 
	centre._x = 100;
	centre._y = 200;
	Couleur c; 
	c._r = 0; 
	c._g = 1; 
	c._b = 0; 
	PolygoneRegulier a(c, centre,  50, 5); 
	a.afficher(); 
	/*Point Or, des;
	Or._x = 0;
	Or._y = 0;
	des._x = 100;
	des._y = 200;
	Couleur c;
	c._r = 1;
	c._g = 0;
	c._b = 0;
    Ligne l(c, Or, des);
    l.afficher(); */
    
    return 0;
}

